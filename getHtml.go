package main

import (
	"fmt"
	"git.oschina.net/Laily/osc-report/check"
	"github.com/PuerkitoBio/goquery"
)

func getDocument() {
	doc, err := goquery.NewDocument("http://www.oschina.com")
	if err != nil {
		fmt.Println(err)
	} else {
		tables := doc.Find("table")
		parseHtml(tables)
	}
}

func parseHtml(s *goquery.Selection) {
	if len(s.Nodes) == 7 {
		var links []check.OSCLink
		for i := 2; i <= 4; i++ {
			table := s.Eq(i)
			table.Find("tr td a").Each(func(a int, sel *goquery.Selection) {
				var link check.OSCLink
				attr, _ := sel.Attr("href")
				link.Link = attr
				link.Content = sel.Text()
				link.Count = 0
				links = append(links, link)
			})
		}
		check.Check(links)
	} else {
		fmt.Printf("table num less: %d", len(s.Nodes))
	}
}

func main() {
	getDocument()
	// check.GetSimilarity("aaa", "aabb")
}
